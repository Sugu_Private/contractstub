package contracts.simple.rest

import org.springframework.cloud.contract.spec.Contract

import static com.sinch.qa.contract.util.grpc.GrpcMetadataConstants.GRPC_STATUS_CODE_NAME
import static com.sinch.qa.contract.util.grpc.GrpcMetadataConstants.GRPC_STATUS_CODE_VALUE

Contract.make {
    description("""
Represents an error scenario
```
given:
	name is set to "noname"
when:
	a request is made with the name
then:
	an error response is received
```

""")
    input {
        messageFrom('grpc:simple.Simpleservice/SayHello')
        messageBody([
                name: 'noname'
        ])
//        messageHeaders {
//            header('sample', 'header')
//        }
    }
    outputMessage {
        sentTo('grpc:simple.Simpleservice/SayHello')
        headers {
            header(GRPC_STATUS_CODE_NAME, 'OUT_OF_RANGE')
            header(GRPC_STATUS_CODE_VALUE, 11)
        }
    }
}
package contracts.simple.rest

import org.springframework.cloud.contract.spec.Contract

import static com.sinch.qa.contract.util.grpc.GrpcMetadataConstants.GRPC_STATUS_CODE_NAME
import static com.sinch.qa.contract.util.grpc.GrpcMetadataConstants.GRPC_STATUS_CODE_VALUE

Contract.make {
    description("""
Represents a success scenario
```
given:
	name is not set to "noname"
when:
	a request is made with the name
then:
	a success response is received
```

""")

    input {
        messageFrom('grpc:simple.Simpleservice/SayHello')
        messageBody([
                name: "good"
        ])
        //messageHeaders {
        //    header('sample', 'header')
        //}
    }
    outputMessage {
        sentTo('grpc:simple.Simpleservice/SayHello')
        body([
                message: 'mock response',
                returnCode: 100
        ])
        headers {
            header(GRPC_STATUS_CODE_NAME, 'OK')
            header(GRPC_STATUS_CODE_VALUE, 0)		}
    }
}